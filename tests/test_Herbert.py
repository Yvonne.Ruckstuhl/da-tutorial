import nose
import numpy
from scripts.Herbert_functions import L2norm, KalmanGain, merge_obs_loc

def test_L2norm():
    arg1 = numpy.zeros((5, 3))
    result = L2norm(arg1)
    numpy.testing.assert_array_equal(numpy.zeros(3), result)

def test_KalmanGain():
    n = 5
    m = 2
    P = numpy.dot(numpy.ones((n,n)),numpy.ones((n,n)))
    R = numpy.identity(m)
    H = numpy.identity(n)[:m,:]
    result = KalmanGain(P,R,H).shape
    numpy.testing.assert_array_equal((n,m), result)
    
def test_Observation():
    n = 40
    u = numpy.arange(0, n, 5)
    h = numpy.arange(0, n, 5)
    result = merge_obs_loc(u, h).shape
    numpy.testing.assert_array_equal(u.shape[0] + h.shape[0], result[0])
"""
ideas:
"""
#1) Test function merge_obs_loc. See Herbert_functions.py for a desciption of what that functions does

#2) Test that the observations are unbiased and have correct standard deviation (Herbert_functions.py: generate_obs() )

#3) write a function that copmutes the background error covariance matrix and replace in the function Herbert_functions.py: update()
# Then test this function, for example by checking that it is symetric and of dimension 2n+1. Also test some specific input/output pairs 

#4) write a test that checks that the trace of the analysis error covariance matrix <= the background error covariance matrix

#5) The eventual goal of data assimilation in NWP is to provide initial conditions (analysis) that lead to a good (long term) forecast. 
# Compute longer forecasts and implement ways to verify the quality of these forecasts. Also design tests for the functions you write!
    

    



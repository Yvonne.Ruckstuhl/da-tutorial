The purpose of this experiment is to get familiar with data assimilation and unit testing.

Docs: documentation

example: a short script that estimates the synthetic temperature on the Zugspitze

scripts: contains scripts necessary for a data assimilation twin experiment with a linear wave model

tests: unit tests for the scripts in scripts/
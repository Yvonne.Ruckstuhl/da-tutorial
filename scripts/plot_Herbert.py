#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 09:43:19 2019

@author: Yvonne.Ruckstuhl
"""

import matplotlib.pyplot as plt
import numpy as np

def plot_P(states,m_const,time):
    j = 0
    index = m_const["index"]
    figs = {}
    for t in time:
        b = np.concatenate((states[j]["bg"][t][index[0],:],states[j]["bg"][t][index[1],:]),axis=0)
        figs[t] = plt.figure()
        plt.imshow(np.cov(b),cmap='hot')
        plt.colorbar()
        plt.title("P for "+str(t)+"th cycle",fontsize=18)
    return figs

def plot_scores(rmse):
    figs = {}
    for dim in ["space","time"]:
        figs[dim], ax = plt.subplots(2,sharex='col', figsize=(10,7))
        for i,var in enumerate(['RMSE over '+dim+' h','RMSE over '+dim+' u']):
            ax[i].set_title(var,fontsize=24)
            ax[i].tick_params(labelsize=18)
            ax[i].plot(rmse[dim]['bg'][var[-1]],'b',label="background")
            ax[i].plot(rmse[dim]['an'][var[-1]],'r',label="analysis")
        ax[0].legend()
        if dim == "space":
            ax[1].set_xlabel("time in assimilation cycles",fontsize=18)
        if dim == "time":
            ax[1].set_xlabel("space in grid points",fontsize=18)
    
    return figs


def snapshot(states,da_const,m_const,time):
    obs_loc = da_const["obs_loc"]
    index = m_const["index"]
    j=0
    
    obspos = obs_loc[obs_loc>0]
    obspos = [(obspos[obspos%2==0]/2-1).astype(int),((obspos[obspos%2==1]-1)/2).astype(int)]
    figs = {}
    for it,t in enumerate(time):
        figs[t], ax = plt.subplots(2,sharex='col', figsize=(12,7))
        for i,var in enumerate(['height h','velocity u']):
            ax[i].set_title(var,fontsize=24)
            ax[i].tick_params(labelsize=18)
            ax[i].plot(states[j]['truth'][t][index[i]],'g',lw=2.5,label='truth')
            ax[i].plot(np.mean(states[j]['bg'][t][index[i],:],axis=1),'b',lw=2.0,label='background')
            ax[i].plot(np.mean(states[j]['an'][t][index[i],:],axis=1),'r',lw=2.0,label='analysis')
            ax[i].plot(obspos[i],states[j]['obs'][t][index[i]][obspos[i]],'go',markersize=7.0,label='observations')
            ax[i].tick_params(labelsize=18)
        ax[0].legend() 
        ax[1].set_xlabel("space in grid points",fontsize=18)
        ax[0].text(0.1, 1.1, str(t)+"th cycle", horizontalalignment='center',  verticalalignment='center', transform=ax[0].transAxes, fontsize=18)

    return figs
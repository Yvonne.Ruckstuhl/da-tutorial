#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 10:22:23 2019

@author: Yvonne.Ruckstuhl
"""

import numpy as np 

def channel(x,dt,std,C,a):
    """
    The state x=[h_0,u_1,h_1,u_2,h_2,...,u_n,h_n], where h_0 are the boundary perturbations 
    is run dt steps forward in time using the model x(t+1)=C*x(t)+a*w 
    where w is a random number drawn from N(0,std).
    """
    for i in range(dt):
        x = np.dot(C,x)
        x = x + a*np.random.normal(0,std,1) 
    return x

def get_analysis(bg,obs,K,H,da_const):
    """
    Computes analysis: an = bg + K(H*bg-obs_pert), where obs_pert are perturbed observations
    """
    obs_pert = np.dot(H,obs+np.random.normal(0,da_const["std_obs"],len(obs)))
    bg_obs = np.dot(H,bg)
    an =  bg + np.dot(K,obs_pert-bg_obs)    
    return an

def KalmanGain(P,R,H):
    """
    Computes the Kalman gain matrix: K = PH^T(HPH^T+R)^(-1)
    """
    P_H = np.dot(P,H.transpose())
    S = np.dot(H,P_H)+R   
    K = np.dot(P_H,np.linalg.inv(S))
    return K

def L2norm(error):
    """
    Computes the l2 norm over the first dimension of the input array
    """
    return np.sqrt(np.average((error) ** 2,axis=0)) 

def merge_obs_loc(obs_loc_h,obs_loc_u):
    """
    merges obs_loc_h corresponding to [h_1,h_2,...,h_n] and obs_loc_u corresponding to [u1,u2,...,u_n]
    to form obs_loc corresponding to the order of the state vector [h_0,u_1,h_1,u_2,h_2,...,u_n,h_n] 
    Note that it is not an option to observe the boundary perturbations h_0.
    example: obs_loc_h  = [0,1,4,n-1] and obs_loc_u = [0,1,3] then obs_loc = [1,2,3,4,7,10,2n]
    """
    obs_loc_u = obs_loc_u*2+1
    obs_loc_h = (obs_loc_h+1)*2
    obs_loc = np.sort(np.concatenate(([obs_loc_h,obs_loc_u])))
    return obs_loc

def set_model_constants(True_std_model=0.1):
    """
    sets constants needed for the model
    """
    const = {}
    const["C"] = np.load("constants/C.npy")            
    const["a"] = np.load("constants/a.npy").reshape(-1)
    const["n"] = int((len(const["a"])-1)/2)                      # number of grid points  
    const["True_std_model"] = True_std_model   # Standard deviation of true model error at the open boundary
    const["index"] = [np.arange(2,2*const["n"]+1,2),np.arange(1,2*const["n"],2)]  # index[0] corresponds to h, index[1] corresponds to u
    return const

def set_constants(ncyc=10,dt=5,std_model=0.1,True_std_obs=0.05,std_obs=0.05,obs_loc_h=np.arange(0,40,5),obs_loc_u=np.arange(0,40,5),nens=100,nexp=10):
    """
    sets constants needed for data assimilation
    """
    obs_loc = merge_obs_loc(obs_loc_h,obs_loc_u) 
    DA_const = {}
    DA_const["ncyc"] = ncyc                         # Number of assimilation cycles
    DA_const["dt"] = dt                           # Number of model timesteps between ovservations
    DA_const["std_model"] = std_model                 # Assumed standard deviation of true model error at the open boundary
    DA_const["True_std_obs"] = True_std_obs             # Standard deviation of true observation error 
    DA_const["std_obs"] = std_obs                  # Assumed Standard deviation of true observation error 
    DA_const["obs_loc"] = obs_loc               # index array of which state elements are observed
    DA_const["nens"] = nens                      # number of ensemble members
    DA_const["nexp"] = nexp                        #number of experiments scores are averaged over 
    return DA_const

def create_states_dict(j,states,m_const,da_const):
    """
    Creates the "states" dictionary where the analysis ensmeble, background ensemble, truth and observations are stored 
    for all assimilation time steps and all experiments. It also randomly initialized the truth and the analysis ensemble 
    """
    n = m_const["n"]
    truth = channel(np.zeros((2*n+1)),40,m_const["True_std_model"],m_const["C"],m_const["a"])
    an = np.zeros((2*n+1,da_const["nens"]))
    for i in range(da_const["nens"]):
        an[:,i] = channel(np.zeros((2*n+1)),40,da_const["std_model"],m_const["C"],m_const["a"])
 
    states[j]={}
    states[j]['bg']=[]
    states[j]['an']=[an]
    states[j]['truth']=[truth]
    states[j]['obs']=[]
    return an,truth,states

def generate_obs(truth,states,da_const,m_const):
    """
    Generates the truth and observations for the next assimilation step
    """
    truth = channel(truth,da_const["dt"],m_const["True_std_model"],m_const["C"],m_const["a"])
    obs = truth + np.random.normal(0,da_const["True_std_obs"],2*m_const["n"]+1)
    states["truth"] = states["truth"]+[truth]
    states["obs"] = states["obs"]+[obs]
    return truth, obs, states  

def predict(analysis,states,da_const,m_const):
    """
    Runs the analysis ensemble forward in time using the model to predict the truth at the next assimilation step
    """
    bg = np.zeros((2*m_const["n"]+1,da_const["nens"]))
    for i in range(da_const["nens"]):
        bg[:,i] = channel(analysis[:,i],da_const["dt"],da_const["std_model"],m_const["C"],m_const["a"])
    states["bg"] = states["bg"]+[bg]
    return bg, states

def update(background,obs,R,H,states,da_const,m_const):
    """
    Computes the analysis by combining the background and observations using their respective corresponding error covariance matrices
    """
     # Compute the background error covariance matrix
    P = np.cov(background)

    # define relative weights of observations and background in terms of the Kalman Gain matrix of size 
    K = KalmanGain(P,R,H)

    # Compute the analysis for each ensemble members
    an = np.zeros((2*m_const["n"]+1,da_const["nens"]))
    for i in range(da_const["nens"]):
        an[:,i] = get_analysis(background[:,i],obs,K,H,da_const)
    states["an"] = states["an"]+[an]
    return an, states   

def get_scores(states,da_const,m_const):
    """
    computes RMSE over space and time respectively, averaged over all experiments for the background ensmeble mean and analysis ensemble mean.
    Structure of the output is as follows: rmse['dim']['quan']['var'] where dim = space or time, quan = bg or an and var = h or u
    """
    index = m_const["index"]
    n = m_const["n"]
    ncyc = da_const["ncyc"]
    nexp = da_const["nexp"]
    time = np.arange(0,ncyc)
    for j in states.keys():
        states[j]["an"] = states[j]["an"][1:]
        states[j]["truth"] = states[j]["truth"][1:]
    rmse = {}
    
    for dim in ['time','space']:
        rmse[dim]={}
        for quan in ['bg','an']:
            rmse[dim][quan]={}
            for i,var in enumerate(["h","u"]):
                rmse[dim][quan][var]={}
                rmse[dim][quan][var]['mean']=0.
            
                for j in states.keys():
                 
                   if dim == "space":
                       rmse[dim][quan][var][j] = np.zeros(ncyc)
                       for t in time:
                           rmse[dim][quan][var][j][t] = L2norm(np.mean(states[j][quan][t][index[i]],axis=1)-states[j]["truth"][t][index[i]]) 
                   if dim == "time":
                       rmse[dim][quan][var][j] = 0.
                       for t in time:
                           rmse[dim][quan][var][j] = rmse[dim][quan][var][j] + (np.mean(states[j][quan][t][index[i]],axis=1)-states[j]["truth"][t][index[i]])**2/float(ncyc) 
                   rmse[dim][quan][var]['mean'] = rmse[dim][quan][var]['mean']+ rmse[dim][quan][var][j]/float(nexp)  
                rmse[dim][quan][var]=rmse[dim][quan][var]['mean']
    return rmse
